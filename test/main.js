jQuery(document).ready(function () {
  //*String, numbers, boolean, null, undefined

  // const name = 'Justinas';
  // const age =  30;

  // const hello = `My name is ${name} and I am ${age}`;

  // console.log('My name is ' + name +  ' and I am ' + age)

  // *Concatenation

  // const s = 'technolog, computers, it, code';

  // console.log(s.split(', '));

  //* Arrays - variablers that hold multiple values
  // const fruits = ['banana', 'clementine', 'pine', 'banana'];

  // fruits.push('cherry')

  // fruits.unshift('melon')

  // fruits.pop();

  // console.log(fruits)

  // console.log(fruits.indexOf('pine'))

  // *Array of objects
  // const todos = [
  //   { id: 1, text: 'Isnesti siukslias', isCompleted: true },
  //   { id: 2, text: 'Susitikimas su kolegom', isCompleted: true },
  //   { id: 3, text: 'Dimplomo iteikimas', isCompleted: false }
  // ];

  // for(let todo of todos) {
  //     console.log(todo.id);
  // }
  // const todoText = todos.map(function(todo) {
  //   return todo.text;
  // const todoCompleted = todos.filter(function(todo) {
  //   return todo.isCompleted === true;
  // }) .map(function(todo) {
  //   return todo.text;
  // })

  // console.log(todoCompleted);

  // console.log(todoText);

  //   console.log(todos)
  // console.log(todos[1].text)
  // const todoJSON =JSON.stringify(todos);
  // console.log(todoJSON);

  ///For
  // for(let i = 0; i <= 10; i++) {
  //     console.log(`For Loop Number: ${i}`)
  // for(let i = 0; i < todos.length; i++) {
  //     console.log(todos[i].text)

  // let i = 0;
  // while(i < 10) {
  //     console.log(`While Loop Number: ${i}`);
  // i++;
  // }

  //*Object literals - key value pairs
  // const person = {
  //     firstName: 'Justinas',
  //     lastName: 'Lukšys',
  //     age: 30,
  //     hobbies: ['hockey', 'movies', 'music', 'wakeboarding'],
  //     address: {
  //         street: 'Sviesos g. 6',
  //         city: 'Elektrenai',
  //         state: 'Lietuva'
  //     }
  // }

  // // console.log(person.firstName)
  // // console.log(person.hobbies[1])
  // // console.log(person.address.city)

  // // const { firstName, lasName } = person ;
  // // console.log(firstName)

  // const { firstName, lasName, address: {city} } = person ;
  // console.log(city)

  // person.email = 'justinas@gmail.com'
  // console.log(person);

  // **CONDITIONALS

  // Simple If/Else Statement
  // const x = 6;
  // const y = 11;

  // if(x > 5 && y > 10) {
  //   console.log('x is more than 5 or y is more than 10');
  // }

  // if(x > 5 || y > 10) {
  //   console.log('x is more than 5 or y is more than 10');
  // }

  // } else if(x > 10) {
  //   console.log('x is greater than 10');
  // } else {
  //   console.log('x is less than 10')
  // }

  //* Switch
  // const x = 9;
  // // const color = x > 10 ? 'red' : 'blue';
  // const color = "green";

  // switch (color) {
  //   case "red":
  //     console.log("color is red");
  //     break;
  //   case "blue":
  //     console.log("color is blue");
  //     break;
  //   default:
  //     console.log("color is NOT red or blue");
  //     break;
  // }

  // Single Element Selectors
// console.log(document.getElementById('my-form'));
// console.log(document.querySelector('.container'));
// Multiple Element Selectors
// console.log(document.querySelectorAll('.item'));
// console.log(document.getElementsByTagName('li'));
// console.log(document.getElementsByClassName('item'));

// const items = document.querySelectorAll('.item');
// items.forEach((item) => console.log(item));


// const ul = document.querySelector('.items');
// ul.remove();
// ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'Hello';
// ul.children[1].innerText = 'Brad';
// ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

// const btn = document.querySelector('.btn');
// btn.style.background = 'red';
// btn.addEventListener('click', e => {
//   e.preventDefault();
//   console.log(e.target.className);
//   document.getElementById('my-form').style.background = '#ccc';
//   document.querySelector('body').classList.add('bg-dark');
//   ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
// });

// const nameInput = document.querySelector('#name');
// nameInput.addEventListener('input', e => {
//   document.querySelector('.container').append(nameInput.value);

  // jQuery( "#datepicker" ).datepicker();

  // jQuery( "#dialog" ).dialog();

  // jQuery('.owl-carousel').owlCarousel({
  //     loop:true,
  //     margin:1,
  //     nav:true,
  //     responsive:{
  //         0:{
  //             items:1
  //         },
  //         600:{
  //             items:2
  //         },
  //         1000:{
  //             items:5
  //         }
  //     }
  // })

    // Get Modal element
    const modal = document.getElementById('simpleModal');
    // Get Modal button
    const modalBtn = document.getElementById('modalBtn');
    //Get close button
    const closeBtn = document.getElementById('closeBtn');

    //Listen for click
    modalBtn.addEventListener('click', openModal);
    closeBtn.addEventListener('click', closeModal);
    window.addEventListener('click', outsideClick);

    function openModal(){
    modal.style.display = 'block';
    }

    function closeModal(){
    modal.style.display = 'none';
    }

    function outsideClick(e){
      if(e.target == modal){
    modal.style.display = 'none';
    }
  }

  // jQuery('.owl-carousel').owlCarousel({
  //   loop:true,
  //   margin:5,
  //   nav:true,
  //   responsive:{
  //       0:{
  //           items:1
  //       },
  //       600:{
  //           items:3
  //       },
  //       1000:{
  //           items:5
  //       }
  //   }
  // }) 
});